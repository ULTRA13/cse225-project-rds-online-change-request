#ifndef INTRO_H
#define INTRO_H
#include <string>

class intro
{
    public:
        inline intro();
        inline std::string display();
        inline void userpage();
        std::string filename;
        std::string username;
        inline std::string name();
        inline void login();
        inline ~intro();
    protected:
    private:
};

#endif // INTRO_H
