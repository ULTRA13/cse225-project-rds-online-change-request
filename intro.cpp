#include "intro.h"
#include "ChangeRequest.cpp"
#include <iostream>
#include <string>
#include <stdlib.h>
#include <conio.h>
#include <fstream>
using namespace std;

string intro::display()
{
    cout<<"#     #     # ####  #     ###   ##   #   #  ####   #######  ##    ###  ###  ###"<<endl;
    cout<<" #   # #   #  ##    #    #     #  #  # # #  ##        #    #  #   # #  #  # #"<<endl;
    cout<<"  # #   # #   #     #    #     #  #  #   #  #         #    #  #   ##   #  #  ##"<<endl;
    cout<<"   #     #    ####  ####  ###   ##   #   #  ####      #     ##    # #  ###  ###"<<endl;
    cout<<"\n\n          LOGIN                                            SIGNUP"<<endl;
    string x;
    cin>>x;
    return x;
}

void intro::userpage()
{
    cout<<"\n\n\n\n\n\n\n\n\n                                USERNAME: ";
    cin>>username;
    cout<<""<<endl;
    cout<<"                                PASSWORD: ";
    string password;
    cin>>password;
    filename = username + password + ".txt";
}

string intro::name()
{
    fstream fout(filename.c_str(), ios::in);
    fout.seekg(67);
    string ch;
    getline(fout,ch);
    fout.close();
    return ch;
}
void intro::login()
{
    int n = 0;
    while (n!=4)
    {
        cout<<"\n\n\n\n\n\n\n                            1)PERSONAL INFORMATION"<<endl;
        cout<<"                            2)CHANGE PASSWORD "<<endl;
        cout<<"                            3)CHANGE PERSONAL INFORMATION "<<endl;
        cout<<"                            4)LOGOUT "<<endl;
        cout<<"\n\n                            ENTER YOUR CHOICE: ";
        cin>>n;
        if (n == 1)
        {
            system("cls");
            fstream fout;
            fout.open(filename.c_str(), ios::in);
            string line;
            while (!fout.eof())
            {
                getline(fout,line);
                cout<<line<<endl;
            }
            fout.close();
            getch();
            system("cls");
        }
        else if (n == 2)
        {
            system("cls");
            cout<<"\n\n\n\n\n\n\n\n           NEW PASSWORD: "<<endl;
            string new1;
            cin>>new1;
            cout<<"           CONFIRM NEW PASSWORD: "<<endl;
            string new2;
            cin>>new2;
            if (new1 == new2)
            {
                fstream fout1(filename.c_str(), ios::in);
                string file = username + new1 + ".txt";
                fstream fout2(file.c_str(), ios::out | ios::trunc);
                string line;
                while (!fout1.eof())
                {
                    getline(fout1,line);
                    fout2<<line<<endl;
                }
                fout1.close();
                remove (filename.c_str());
                fout2.close();
            }
        }
        else if (n == 3)
        {
             system("cls");
             cout<<"\n\n\n\n\n                              WHICH ONE WOULD YOU LIKE TO CHANGE?"<<endl;
             cout<<"                              1)STUDENT'S NAME"<<endl;
             cout<<"                              2)PHONE NUMBER"<<endl;
             cout<<"                              3)MAILING ADDRESS"<<endl;
             cout<<"                              4)GENDER"<<endl;
             cout<<"                              5)PARENT'S NAME"<<endl;
             int x;
             cin>>x;
             changeRequest(x);
        }
    }
}

intro::intro()
{
    //ctor
}

intro::~intro()
{
    //dtor
}
