#include "intro.h"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <conio.h>
using namespace std;

inline string addUser_intoFile()
{
    string id;
    cout<<"I.D.# ";
    cin>>id;
    cout<<""<<endl;
    string password;
    cout<<"PASSWORD: ";
    cin>>password;
    cout<<""<<endl;
    string file = id + password + ".txt";
    ofstream fout;
    fout.open(file.c_str(), ios::out);
    fout <<"                              PERSONAL INFORMATION\n\n"<< endl;
    string name;
    cout<<"FULL NAME: ";
    cin.ignore();
    getline(cin, name);
    cout<<""<<endl;
    fout <<"FULL NAME: "<<name<< endl;
    string phone;
    cout<<"CELLPHONE: ";
    cin>>phone;
    cout<<""<<endl;
    fout <<"CELLPHONE: "<<phone<< endl;
    string email;
    cout<<"EMAIL ADDRESS: ";
    cin>>email;
    cout<<""<<endl;
    fout <<"EMAIL ADDRESS: "<<email<< endl;
    string dateofbirth;
    cout<<"DATE OF BIRTH: ";
    cin.ignore();
    getline(cin,dateofbirth);
    cout<<""<<endl;
    fout <<"DATE OF BIRTH: "<<dateofbirth<< endl;
    string sex;
    cout<<"SEX: ";
    cin>>sex;
    cout<<""<<endl;
    fout <<"SEX: "<<sex<< endl;
    string citizen;
    cout<<"CITIZENSHIP: ";
    cin>>citizen;
    cout<<""<<endl;
    fout <<"CITIZENSHIP: "<<citizen<< endl;
    string blood;
    cout<<"BLOOD GROUP: ";
    cin>>blood;
    cout<<""<<endl;
    fout <<"BLOOD GROUP: "<<blood<< endl;
    string status;
    cout<<"MARITAL STATUS: ";
    cin>>status;
    cout<<""<<endl;
    fout <<"MARITAL STATUS: "<<status<< endl;
    string mailaddress;
    cout<<"MAILING ADDRESS: ";
    cin.ignore();
    getline(cin,mailaddress);
    cout<<""<<endl;
    fout <<"MAILING ADDRESS: "<<mailaddress<< endl;
    fout <<"\n\n                              PARENTAL INFORMATION\n\n"<< endl;
    string father;
    cout<<"FATHER'S NAME: ";
    getline(cin,father);
    cout<<""<<endl;
    fout <<"FATHER'S NAME: "<<father<< endl;
    string mother;
    cout<<"MOTHER'S NAME: ";
    getline(cin,mother);
    cout<<""<<endl;
    fout <<"MOTHER'S NAME: "<<mother<< endl;
    string guardian;
    cout<<"GUARDIAN'S NAME: ";
    getline(cin,guardian);
    cout<<""<<endl;
    fout <<"GUARDIAN'S NAME: "<<guardian<< endl;
    string perm_address;
    cout<<"PERMANENT ADDRESS: ";
    getline(cin,perm_address);
    cout<<""<<endl;
    fout <<"PERMANENT ADDRESS: "<<perm_address<< endl;
    string guardian_phone;
    cout<<"GUARDIAN'S MOBILE NUMBER: ";
    getline(cin,guardian_phone);
    cout<<""<<endl;
    fout <<"GUARDIAN'S MOBILE NUMBER: "<<guardian_phone<< endl;

    fout.close();
    return file;
}
